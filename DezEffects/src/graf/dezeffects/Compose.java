/**
 * 
 */
package graf.dezeffects;

import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class Compose {
	
	public Compose() {
	}

	/**
	 * @param fluidImage
	 * @param frame
	 * @return
	 */
	public static PImage ifBrighterThan(int br0, int br1, PImage src, PImage dst) {
		PImage out = new PImage(src.width,src.height);
		int dstBright;
		int a, r, g, b;
		float amount;
		for (int i=0;i<dst.pixels.length;i++) {
			dstBright = ((dst.pixels[i]>>16&0xff)+(dst.pixels[i]>>8&0xff)+(dst.pixels[i]&0xff))/3;
			if (dstBright>br0) {
				amount = (src.pixels[i]>>24&0xff)/255.0f;
				amount *= Math.min( ((float)dstBright-br0)/(br1-br0), 1 );
				out.pixels[i] = mix(src.pixels[i],dst.pixels[i],amount);
			} else {
				out.pixels[i]=dst.pixels[i];
			}
		}
		return out;
	}
	
	private static int mix(int src, int dst, float amount) {
		float da = 1-amount;
		int r = (int)(amount*(src>>16&0xff)+da*(dst>>16&0xff));
		int g = (int)(amount*(src>>8&0xff)+da*(dst>>8&0xff));
		int b = (int)(amount*(src&0xff)+da*(dst&0xff));
		return 0xff<<24|r<<16|g<<8|b;
	}
}
