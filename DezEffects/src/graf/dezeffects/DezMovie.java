/**
 * 
 */
package graf.dezeffects;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class DezMovie {
	
	public static final int ALL = Integer.MAX_VALUE;
	
//	private final String path = "D:\\_PROJECTS\\Video Effects Dez\\frames\\"; //50pBW
//	private final String name = "StukjeDez";
//	private final String ext = ".bmp";
	
	private final String path = "inputmovie/MarcusMotion/"; //50pBW
	private final String name = "MarcusMotion_";
	private final String ext = ".jpg";
	
	//180..340
	//530..600
	private final int frameFirst = 0;//180;
	private final int frameLast = 259;//340;
	
	public final int width, height;
	public final int length;
	
	private final PApplet pa;
	private final PImage[] img;
	
	private boolean backAndForth=false;
	private int stepDirection=1;
	
	private int currentFrame=0;
	
	public DezMovie(PApplet pa) {
		this.pa = pa;
		img = new PImage[frameLast-frameFirst];
		loadFrame(0);
		width = img[0].width;
		height = img[0].height;
		length = frameLast-frameFirst;
	}
	
	public PImage getFrame() {
		if (img[currentFrame]==null) {
			loadFrame(currentFrame);
		}
		try {
			PImage out = (PImage)img[currentFrame].clone();
			return out;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
		//return (PImage)img[currentFrame].clone();
	}
	
	private void loadFrame(int frame) {
		String loadName = name + PApplet.nf(frameFirst+frame,4) + ext;
		System.out.println("loading frame: "+loadName);
		img[frame] = pa.loadImage(path + loadName);
		////10/10 223/16
		//ffffef
//		img[frame].copy(img[frame], 0, 0, 1, 1, 10, 10, 223, 16);
	}
	
	public void loop() {
		backAndForth=false;
	}
	
	public void loopBackAndForth() {
		backAndForth=true;
	}
	
	public void preload(int nrOfFrames) {
		nrOfFrames = Math.min(nrOfFrames,length);
		for (int i=0;i<nrOfFrames;i++) {
			loadFrame(i);
		}
	}
	
	public void step() {
		
		if (backAndForth) {
			currentFrame+=stepDirection;
			if (currentFrame==length) {
				currentFrame=length-2;
				stepDirection=-1;
			}
			else if (currentFrame==-1) {
				currentFrame=1;
				stepDirection=1;
			}
		} else {
			currentFrame++;
			if (currentFrame==length) {
				currentFrame=0;
			}
		}
		
		System.out.println(this+"step() "+currentFrame);
	}
	
}
