/**
 * 
 */
package graf.dezeffects;

import java.util.Arrays;

import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class Median {

	private static class PixInfo implements Comparable {
		int col, br;
		PixInfo(int col) {
			this.col = col;
			br = (col>>16&0xff)+(col>>8&0xff)+(col&0xff);
		}
		public int compareTo(Object arg0) {
			PixInfo other = (PixInfo)arg0;
			if (other.br<br) {
				return -1;
			} else if (other.br>br) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	
	public static void filter(PImage img, int side) {
		
		int[] out = new int[img.pixels.length];
		int width = img.width;
		int height = img.height;
		int len = 2 * side + 1;
		int lenSq = len * len;
		int halfArray = lenSq / 2;

		/*
		 *  create window
		 */
		int[] windowIndex = new int[lenSq];
		int[] xo = new int[lenSq];
		int[] yo = new int[lenSq];
		int fi = 0;
		for (int y = -side; y <= side; y++) {
			for (int x = -side; x <= side; x++) {
				windowIndex[fi] = y * img.width + x;
				xo[fi] = x;
				yo[fi] = y;
				fi++;
			}
		}

		/*
		 *  Slide window
		 */
		int srcIndex = 0;
		int dstIndex = 0;
		//int[] window = new int[lenSq];
		PixInfo[] window = new PixInfo[lenSq];
		
		for (int y = 0; y < img.height; y++) {
			for (int x = 0; x < img.width; x++) {

				for (int i = 0; i < windowIndex.length; i++) {

					srcIndex = dstIndex + windowIndex[i];
					if (x + xo[i] < 0) {
						srcIndex -= xo[i];
					} else if (x + xo[i] > width - 1) {
						srcIndex -= xo[i];
					}
					if (y + yo[i] < 0) {
						srcIndex -= yo[i] * width;
					} else if (y + yo[i] > height - 1) {
						srcIndex -= yo[i] * width;
					}

					//try {
					//window[i] = (img.pixels[srcIndex] & 0xff);
					window[i] = new PixInfo(img.pixels[srcIndex]);
					//} catch (ArrayIndexOutOfBoundsException e) {
					/// e.printStackTrace();
					//  println(x+"/"+y+", offset "+xo[i]+"/"+yo[i]+" index "+i+" -> "+srcIndex);
					//  System.exit(0);
					//}
				}

				Arrays.sort(window);
				/*out[dstIndex] = window[halfArray] << 16
						| window[halfArray] << 8 | window[halfArray];*/

				out[dstIndex] = window[halfArray].col;
				
				dstIndex++;
			}
		}

		img.pixels = out;
		img.updatePixels();
		
	}
}
