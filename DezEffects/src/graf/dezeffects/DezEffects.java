/**
 * 
 */
package graf.dezeffects;

import java.awt.Rectangle;

import graf.gamefluids.FluidBox;
import graf.gamefluids.FluidRect;
import graf.gradient.ColorGradient;
import graf.opticalflow.FlowField;
import processing.core.*;

/**
 * @author Marcus
 *
 */
public class DezEffects extends PApplet {
	
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.dezeffects.DezEffects"});
	}
	
	final float vizSpace = 5;
	
	float ff_lambda = 0.5f; // was 0.2f for small image
	float ff_blurFac = 0f; // was 0.1f for small image
	float ff_thresh = 0.00003f; // was 0.00003f for small image
	
	float ff_to_densities = 4000;
	float ff_to_velocities = 500;
	
	float densityMax = 0.3f; // was 0.1f for small image
	
	float dt = 1.0f;
	float diffusion = 0.00001f;
	float viscosity = 0.00001f;
	float evaporation = 0.035f; //0.008
	
	String outputFileRoot = "output/DezEffectsOutput_";
	boolean saveOutput = false;
	
	Rectangle[] ff_ignore = null;
	
	DezMovie mov;
	PImage frame0, frame1;
	FluidRect fluid;
	
	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		mov = new DezMovie(this);
		mov.loopBackAndForth();
		mov.preload(DezMovie.ALL);
		
		FlowField.lambda = ff_lambda;
		FlowField.blurFac = ff_blurFac;
		/*ff_ignore = new Rectangle[] {
				new Rectangle(mov.width/2-25,0,40,30),
				new Rectangle(mov.width/2-30,50,50,mov.height-50)
		}; // for small mov
		
*/		
//		ff_ignore = new Rectangle[] {
//				new Rectangle(mov.width/2-50,0,80,60),
//				new Rectangle(mov.width/2-60,100,100,mov.height-100)
//		}; // for small mov
		
		fluid = new FluidRect(mov.width, mov.height);
	}
	
	public void draw() {
		background(32,32,32);
		
		frame0 = frame1;
		frame1 = mov.getFrame();
		
		Median.filter(frame1, 1);
		Median.filter(frame1, 1);
		
		if (frame0!=null) {
			
			FlowField.calculate(frame0.pixels, frame1.pixels, frame0.width, frame0.height);
			FlowField.thresh(ff_thresh);
			if (ff_ignore!=null)
				for (int i=0;i<ff_ignore.length;i++) {
					FlowField.deleteRect(ff_ignore[i].x,ff_ignore[i].y,ff_ignore[i].width,ff_ignore[i].height);
				}
			
			float[] inputDensities = new float[fluid.size];
			float[] inputVelocitiesX = new float[fluid.size];
			float[] inputVelocitiesY = new float[fluid.size];
			
			int i=0, x, y;
			for (y=0;y<FlowField.height;y++) {
				for (x=0;x<FlowField.width;x++) {
					inputDensities[i] = Math.min(densityMax,ff_to_densities*FlowField.velocityMagnitude[i]);
					inputVelocitiesX[i] = ff_to_velocities*FlowField.velocityX[i];
					inputVelocitiesY[i] = ff_to_velocities*FlowField.velocityY[i];
					if (inputDensities[i]>0) {
						inputVelocitiesY[i] += 0.001f;
					}
					i++;
				}
			}
			
			fluid.evaporate(evaporation, dt);
			fluid.step(inputDensities, inputVelocitiesX, inputVelocitiesY, diffusion, viscosity, dt);
		}
		
		/*
		 * Show movie frame
		 */
		//fill(255);
		image(mov.getFrame(),vizSpace,vizSpace);
		image(frame1,vizSpace,mov.height+2*vizSpace);
		
		
		/*
		 * Show FlowField
		 */
		float sx = mov.width+2*vizSpace;
		float sy = vizSpace;
		float ss = 1;
		drawField(
				sx, sy, ss,
				FlowField.width, FlowField.height, 
				FlowField.velocityX, FlowField.velocityY, 60000);
		
		/*
		 * Show ignore ff's
		 */
		if (ff_ignore!=null)
			for (int i=0;i<ff_ignore.length;i++) {
				rectMode(CORNER);
				stroke(255,0,0);
				//noFill();
				rect(sx+ff_ignore[i].x*ss, sy+ff_ignore[i].y*ss, 
						ff_ignore[i].width*ss, ff_ignore[i].height*ss);
			}
		//fill(255);
		
		/*
		 * Show fluid
		 */
		//image(mov.getFrame(),mov.width+2*vizSpace, vizSpace + 2*mov.height + vizSpace);
		
		PImage fluidImage = new PImage(fluid.WIDTH,fluid.HEIGHT);
		fluidImage.format = ARGB;
		for (int i=0;i<fluidImage.pixels.length;i++) {
			fluidImage.pixels[i] = ColorGradient.BLACK_SMOKE.getColorAt(fluid.getDensity()[i]);
		}
		
		
		//PImage finalImage = Compose.ifBrighterThan(190,210,fluidImage,mov.getFrame());
		
		//image(fluidImage,mov.width+2*vizSpace, vizSpace + mov.height + vizSpace);
		
		
		PImage finalImage;
		try {
			finalImage = (PImage)mov.getFrame().clone();
			finalImage.blend(fluidImage, 0, 0, mov.width, mov.height, 0, 0, mov.width, mov.height, BURN);
			image(finalImage,mov.width+2*vizSpace, vizSpace + mov.height + vizSpace);
			if (saveOutput) {
				String name = outputFileRoot+nf(frameCount,4)+".jpg";
				println("Saving to "+name);
				finalImage.save(name);
			}
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*drawFluid(
				mov.width+2*vizSpace, vizSpace + 2*mov.height + vizSpace, 1,
				fluid.WIDTH, fluid.HEIGHT,
				fluid.getDensity());*/
		
		
		mov.step();
		mov.step();
	}

	/**
	 * @param f
	 * @param g
	 * @param width
	 * @param height
	 * @param density
	 */
	private void drawFluid(float screenX, float screenY, float scale,  
			int width, int height,
			float[] density) {
		int i=0, x, y, col;
		rectMode(CORNER);
		//stroke(0);
		//noFill();
		//fill(255);
		//rect(screenX,screenY,width*scale,height*scale);
		
		noStroke();
		
		for (y=0;y<height;y++) {
			for (x=0;x<width;x++) {
				col = ColorGradient.RED_SMOKE.getColorAt(density[i]);
				fill(col);
				rect(screenX + x*scale, screenY + y*scale, scale, scale);
				i++;
			}
		}
	}

	/**
	 * @param f
	 * @param vizSpace2
	 * @param visWidth
	 * @param visHeight
	 * @param velocityX
	 * @param velocityY
	 */
	private void drawField(
			float visX, float visY, float visScale,
			int width, int height, 
			float[] velocityX, float[] velocityY,
			float velocityVisScale) {
		
		stroke(0,0,0);
		fill(255,255,255);
		rectMode(CORNER);
		rect(visX,visY,width*visScale,height*visScale);
		int i=0;
		float x0,y0,x1,y1;
		for (int y=0;y<height;y++) {
			y0 = visY + y*visScale;
			for (int x=0;x<width;x++) {
				x0 = visX + x*visScale;
				x1 = x0 + velocityX[i]*velocityVisScale;
				y1 = y0 + velocityY[i]*velocityVisScale;
				if (x1!=x0||y1!=y0) {
					line(x0,y0,x1,y1);
				}
				i++;
			}
		}
		
	}
}
