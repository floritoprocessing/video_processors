package convolution3d;

import processing.core.PImage;

public class Convolver {

	final int width, height, planes;
	final int planeSize;
	final int yToIndex, zToIndex, amount;	
	
	int[] pixels;
	int[] tempPixels;
	
	final int[] xo, yo, zo;
	final int[] kernelOffset;
	
	
	static float[] normalize(float[] kernel) {
		float total = 0;
		for (int i=0;i<kernel.length;i++) {
			total+=kernel[i];
		}
		if (total!=0) {
			float fac = 1.0f/total;
			for (int i=0;i<kernel.length;i++) {
				kernel[i]*=fac;
			}
		}
		return kernel;
	}
	
	static final float part = 1.0f/27;
	static final float[] meanSpaceAndTimeFilter = normalize(new float[] {
			1,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1
	});
	
	static final float[] meanTimeFilter = normalize(new float[] {
			0, 0, 0, 0, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0
		});
	
	static final float[] gaussSpaceAndTimeFilter = normalize(new float[] {
			1,2,1, 2,4,2, 1,2,1,
			2,4,2, 4,8,4, 2,4,2,
			1,2,1, 2,4,2, 1,2,1,
	});
	static final float[] gaussTimeFilter = normalize(new float[] {
		0,0,0, 0,1,0, 0,0,0,
		0,0,0, 0,2,0, 0,0,0,
		0,0,0, 0,1,0, 0,0,0
	});
	
	
	
	
	
	static final float[] timeEdgeFilter1 = normalize(new float[] {
		0, 0, 0, 0, 1, 0, 0, 0, 0,
		0, 0, 0, 0, -2, 0, 0, 0, 0,
		0, 0, 0, 0, 1, 0, 0, 0, 0
	});
	
	static final float[] timeEdgeFilter2 = normalize(new float[] {
			0, 0, 0, 0, 1, 0, 0, 0, 0,
			0, 0, 0, 0, -10, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0
		});
	
	static final float[] timeAndSpaceEdgeFilter = normalize(new float[] {
			0,0,0, 0,1,0,  0,0,0,
			0,1,0, 1,-6,1, 0,1,0,
			0,0,0, 0,1,0,  0,0,0
	});
	
	static final float[] spaceSharpness = normalize(new float[] {
		0,0,0,0,0,0,0,0,0,
		-1, -1, -1, -1,  9, -1, -1, -1, -1,
		0,0,0,0,0,0,0,0,0,
	});
	
	static final float[] time_Y_Sharpness = normalize(new float[] {
			0,-1,0,0,-1,0,0,-1,0,
			0,0,0,0,9,0,0,0,0,
			0,-1,0,0,-1,0,0,-1,0
		});
	
	// relief:
	// 2  0  0
	// 0 -1  0
	// 0  0 -1
	
	//TODO
	/*
	 * Sobel edge detection
	 * 
	 * -1 0 1
	 * -2 0 2
	 * -1 0 1
	 * 
	 * and then
	 * 
	 * 1 2 1
	 * 0 0 0
	 * -1 2 1
	 * 
	 */
	
	public Convolver(int width, int height, int planes) {
		this.width = width;
		this.height = height;
		this.planes = planes;
		yToIndex = width;
		planeSize = zToIndex = width*height;
		amount = width*height*planes;
		
		kernelOffset = new int[27];
		xo = new int[27];
		yo = new int[27];
		zo = new int[27];
		int index=0;
		for (int z=-1;z<=1;z++) {
			for (int y=-1;y<=1;y++) {
				for (int x=-1;x<=1;x++) {
					kernelOffset[index] = z*zToIndex + y*yToIndex + x;
					xo[index] = x;
					yo[index] = y;
					zo[index] = z;
					index++;
				}
			}
		}
		pixels = new int[amount];
		tempPixels = new int[amount];
	}
	
	
	public void clipEdgeConvolution(float[] kernel) {
		int sourceIndex;
		int sourcePixel;
		int sourceR, sourceG, sourceB;
		float r, g, b;
		int sx, sy, sz;
		System.arraycopy(pixels, 0, tempPixels, 0, amount);
		
		int targetIndex = 0;
		for (int z=0;z<planes;z++) {
			for (int y=0;y<height;y++) {
				for (int x=0;x<width;x++) {
					//targetIndex = z*zToIndex + y*yToIndex + x;
					
					r = g = b = 0;
					for (int kIndex=0;kIndex<27;kIndex++) {
						sourceIndex = targetIndex + kernelOffset[kIndex];
						sx = x+xo[kIndex];
						sy = y+yo[kIndex];
						sz = z+zo[kIndex];
						if (sx<0) {
							sourceIndex++;
						} else if (sx>=width) {
							sourceIndex--;
						}
						if (sy<0) {
							sourceIndex+=yToIndex;
						} else if (sy>=height) {
							sourceIndex-=yToIndex;
						}
						if (sz<0) {
							sourceIndex+=zToIndex;
						} else if (sz>=planes){
							sourceIndex-=zToIndex;
						}
						sourcePixel = pixels[sourceIndex];
						sourceR = sourcePixel>>16&0xff;
						sourceG = sourcePixel>>8&0xff;
						sourceB = sourcePixel&0xff;
						r += kernel[kIndex] * sourceR;
						g += kernel[kIndex] * sourceG;
						b += kernel[kIndex] * sourceB;
					}
					r = Math.max(0, Math.min(255, r));
					g = Math.max(0, Math.min(255, g));
					b = Math.max(0, Math.min(255, b));
					tempPixels[targetIndex] = 0xff000000 | ((int)r)<<16 | ((int)g)<<8 | ((int)b);
					targetIndex++;
					
				}
			}
		}
		
		System.arraycopy(tempPixels, 0, pixels, 0, amount);
	}
	
	
	
	public void noEdgeConvolution(float[] kernel) {
		int targetIndex;
		int sourceIndex;
		int sourcePixel;
		int sourceR, sourceG, sourceB;
		float r, g, b;
		
		System.arraycopy(pixels, 0, tempPixels, 0, amount);
		
		for (int z=1;z<planes-1;z++) {
			for (int y=1;y<height-1;y++) {
				for (int x=1;x<width-1;x++) {
					targetIndex = z*zToIndex + y*yToIndex + x;
					
					r = g = b = 0;
					for (int i=0;i<27;i++) {
						sourceIndex = targetIndex + kernelOffset[i];
						sourcePixel = pixels[sourceIndex];
						sourceR = sourcePixel>>16&0xff;
						sourceG = sourcePixel>>8&0xff;
						sourceB = sourcePixel&0xff;
						r += kernel[i] * sourceR;
						g += kernel[i] * sourceG;
						b += kernel[i] * sourceB;
					}
					r = Math.max(0, Math.min(255, r));
					g = Math.max(0, Math.min(255, g));
					b = Math.max(0, Math.min(255, b));
					tempPixels[targetIndex] = 0xff000000 | ((int)r)<<16 | ((int)g)<<8 | ((int)b);
					
				}
			}
		}
		
		System.arraycopy(tempPixels, 0, pixels, 0, amount);
		
	}
	
	public void setPlane(int zPlane, int[] pixelArray) {
		if (pixelArray.length!=planeSize) {
			throw new RuntimeException("pixel array wrong length!");
		}
		else if (zPlane<0||zPlane>=planes) {
			throw new RuntimeException("Illegal plane");
		}
		System.arraycopy(pixelArray, 0, pixels, zPlane*zToIndex, planeSize);
	}
	
	public void getPlane(int zPlane, PImage targetImage) {
		if (zPlane<0||zPlane>=planes) {
			throw new RuntimeException("Illegal plane");
		}
		if (targetImage.width!=width || targetImage.height!=height) {
			throw new RuntimeException("Illegal image");
		}
		System.arraycopy(pixels, zPlane*zToIndex, targetImage.pixels, 0, planeSize);
		targetImage.updatePixels();
	}
	
}
