package convolution3d;

import processing.core.PApplet;
import processing.core.PImage;


public class Convolution3D extends PApplet {
	
	//int numberOfFrames = 25; int WIDTH = 320, HEIGHT = 240; String name = "Garbage"; int frameOffset = 1; int digits = 2; String extension = ".bmp";
	//int numberOfFrames = 50; int WIDTH = 480, HEIGHT = 270; String name = "GRW101/GRW101_"; int frameOffset = 100; int digits = 5; String extension = ".png";
	int numberOfFrames = 75; int WIDTH = 300, HEIGHT = 169; String name = "GRW126short/GRW126_"; int frameOffset = 0; int digits = 5; String extension = "_1.png";
	
	
	Convolver convolver = new Convolver(WIDTH, HEIGHT, numberOfFrames);
	PImage img;
	boolean nextFrameConvolve = false;
	boolean saveOutputMovies = false;
	int convolutionLevel = 0;
	
	public void settings() {
		size(WIDTH,HEIGHT,P3D);
	}
	
	public static void main(String[] args) {
		PApplet.main("convolution3d.Convolution3D");
	}
	
	public void setup() {
		img = new PImage(WIDTH, HEIGHT);
		
		for (int i=0;i<numberOfFrames;i++) {
			//String name = "GRW101/GRW101_"+nf(i+1,5)+".png";
			//String name = "Garbage"+nf(i+1,2)+".bmp";
			String filename = name+nf(i+frameOffset,digits)+extension;
			println("loading "+filename);
			PImage img = loadImage(filename);
			convolver.setPlane(i, img.pixels);
		}
		
				
		frameRate(25);
	}

	public void keyPressed() {
		nextFrameConvolve = true;
	}
	
	public void draw() {
		if (nextFrameConvolve) {
			int amount = numberOfFrames;
			println("CONVOLVING! ("+amount+"x)");
			for (int i=0;i<amount;i++) {
				println((i+1)+"/"+amount);
				convolver.noEdgeConvolution(Convolver.gaussTimeFilter);
			}
			
			
			convolutionLevel++;
			
			if (saveOutputMovies) {
				String basePath = "output/lev_"+nf(convolutionLevel,3)+"/lev_frames_";
				for (int i=0;i<amount;i++) {
					String path = basePath+nf(i,4)+".jpg";
					convolver.getPlane(i, img);
					println("saving "+path);
					img.save(sketchPath(path));
					
//					save(path);
				}
			}
			
			nextFrameConvolve = false;
		}
		/*float movieTime = ((frameCount-1)/(float)convolver.planes*2)%2; // 0..2;
		movieTime -= 0.5f; // -0.5..1.5
		movieTime = Math.max(0, Math.min(1, movieTime));
		int frame = (int)((convolver.planes-1)*movieTime);	*/
		int frame = (frameCount-1)%convolver.planes;
		convolver.getPlane(frame, img);
		image(img,0,0);
	}
}
