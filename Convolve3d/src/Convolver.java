import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Convolver {
	
	static float[] gaussKernel = new float[] {
		-1,-2,-1,
		-2,-4,-2,
		-1,-2,-1,
		
		2,4,2,
		4,4,4,
		2,4,2,
		
		-1,-2,-1,
		-2,-4,-2,
		-1,-2,-1
	};
	
	static float[] lineTimeAKALimeKernelColonel = new float[] {
		0,-1,0,
		0,-1,0,
		0,-1,0,
		
		0,1,0,
		0,-2,0,
		0,1,0,
		
		0,1,0,
		0,1,0,
		0,1,0,
	};
	
	static float[] tomWaitsKernel = new float[] {
		-1,-1,-1,
		-1,-1,-1,
		-1,-1,-1,
		
		0,0,0,
		0,0,0,
		0,0,0,
		
		1,1,1,
		1,1,1,
		1,1,1,
	};
	
	static float[] coolKernel = new float[] {
		0,0,0,
		0,0,0,
		0,0,0,
		
		0,0,0,
		0,0,0,
		0,0,0,
		
		0,0,0,
		0,0,0,
		0,0,0,
	};
	
	static void createNewRandomKernel() {
		for (int i=0;i<27;i++) {
			randomKernel[i] = (float)(Math.random()*2-1);
			System.out.print(randomKernel[i]+"\t");
			if (i%3==2) {
				System.out.println();
			}
			if (i==8||i==17) {
				System.out.println();
			}
		}
		
	}
	static float[] randomKernel = new float[27];
	static {
		createNewRandomKernel();
	}
	
	
	
	
	int width, height, frames;
	int yToIndex, zToIndex, imageSize;
	int[] origPixels, newPixels;
	int[] indexOffsets;
	float[] kernel = randomKernel;//lineTimeAKALimeKernelColonel;//tomWaitsKernel;//lineTimeAKALimeKernelColonel;//gaussKernel;
	
	public Convolver(PImage[] original) {
		this.width = original[0].width;
		this.height = original[0].height;
		this.frames = original.length;
		yToIndex = width;
		zToIndex = width*height;
		imageSize = width*height;
		origPixels = new int[width*height*frames];
		newPixels = new int[width*height*frames];
		
		indexOffsets = new int[] {
				-zToIndex-width-1, -zToIndex-width, -zToIndex-width+1,
				-zToIndex-1, -zToIndex, -zToIndex+1,
				-zToIndex+width-1, -zToIndex+width, -zToIndex+width+1,
				
				-width-1, -width, -width+1,
				-1, 0, 1,
				width-1, width, width+1,
				
				zToIndex-width-1, zToIndex-width, zToIndex-width+1,
				zToIndex-1, zToIndex, zToIndex+1,
				zToIndex+width-1, zToIndex+width, zToIndex+width+1,
		};
		
		
		float part = 1.0f/27f;
		/*kernel = new float[27];
		for (int i=0;i<27;i++) {
			kernel[i] = part;			
		}
		kernel[13] = -1*part;*/
		
		//add up all the parts
		float total = 0;
		for (int i=0;i<27;i++) {
			total += kernel[i];
		}
		//System.out.println(total);
		// and normalizes
		if (total!=0) {
			for (int i=0;i<27;i++) {
				kernel[i] /= total;
			}
		}
		
		
		int index=0;
		for (int i=0;i<frames;i++) {
			original[i].loadPixels();
			for (int j=0;j<imageSize;j++) {
				origPixels[index] = original[i].pixels[j];//&0xff;
				index++;
			}
		}
		System.arraycopy(origPixels, 0, newPixels, 0, imageSize*frames);
	}
	
	public int[] getPixels(int imageIndex) {
		int[] out = new int[imageSize];
		//int origIndex = imageIndex*imageSize;
		/*int gray;
		for (int j=0;j<imageSize;j++) {
			gray = newPixels[origIndex];
			out[j] = gray<<16 | gray<<8 | gray;
			origIndex++;
		}*/
		System.arraycopy(newPixels, imageIndex*imageSize, out, 0, imageSize);
		return out;
	}
	
	public void doIt() {
		
		int targetIndex, zIndex, yIndex;
		int indexOffset;
		int sourceIndex;
		float gray;
		float r, g, b;
		//int gray;
		
		for (int z=1;z<frames-1;z++) {
			zIndex = z*zToIndex;
			for (int y=1;y<height-1;y++) {
				yIndex = y*yToIndex;
				for (int x=1;x<width-1;x++) {
					targetIndex = zIndex+yIndex+x;
					
					r=0;
					g=0;
					b=0;
					for (int o=0;o<27;o++) {
						indexOffset = indexOffsets[o];
						sourceIndex = targetIndex+indexOffset;
						r += kernel[o]*(origPixels[sourceIndex]>>16&0xff);
						g += kernel[o]*(origPixels[sourceIndex]>>8&0xff);
						b += kernel[o]*(origPixels[sourceIndex]&0xff);
						//gray += origPixels[sourceIndex];
					}
					//gray /=27;
					r = Math.max(0, Math.min(255, r));
					g = Math.max(0, Math.min(255, g));
					b = Math.max(0, Math.min(255, b));
					newPixels[targetIndex] = ((int)r)<<16|((int)g)<<8|((int)b);
					//Math.max(0, Math.min(255,gray));
					
				}
			}
		}
		
		System.arraycopy(newPixels, 0, origPixels, 0, newPixels.length);
		
	}
	
	
}
