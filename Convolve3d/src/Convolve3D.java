import java.io.File;
import java.util.Arrays;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Convolve3D extends PApplet {
	
	//String pathToMov = "D:\\My Documents\\tring-tring STRP\\Recording 20101113\\Recording 20101113";
	String pathToMov = "garbage";//D:\\My Documents\\Processing\\_Eclipse\\Convolve3d\\garbage\\";
	
	public static void main(String[] args) {
		PApplet.main(new String[] {"Convolve3D"});
	}
	
	File dir = new File(pathToMov);
	File[] files = dir.listFiles();
	int amountOfImages = 15;
	PImage[] image = new PImage[amountOfImages];
	PImage showImage;
	boolean doIt = false;
	
	Convolver convolver;
	
	public void keyPressed() {
		if (key=='c') {
			doIt = true;
		}
	}
	
	public void settings() {
		size(320,240);
	}
	
	public void setup() {
		
		Arrays.sort(files);
		for (int i=0;i<amountOfImages;i++) {
			image[i] = loadImage(files[i].getAbsolutePath());
		}
		convolver = new Convolver(image);
		showImage = new PImage(320,240);
		frameRate(15);
	}
	
	public void draw() {
		int frame = frameCount%(amountOfImages*2);
		if (frame>=amountOfImages) {
			frame = 2*amountOfImages-frame-1;
		}
		//frame -= amountOfImages/2;
		//frame = Math.max(0,Math.min(amountOfImages-1,frame));
		//image(image[frame],0,0);
		
		showImage.pixels = convolver.getPixels(frame);
		showImage.updatePixels();
		image(showImage,0,0);
		
		if (doIt) {
			//Convolver.createNewRandomKernel();
			convolver.doIt();
			doIt=false;
			println("done");
		}
		
	}
}
