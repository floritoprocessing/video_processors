class Blob {
  
  float R=3;
  float TRANS = 255;
  float TRANSADD = -5;
  float RED=255;
  float GREEN=255;
  float BLUE=255;
  
  float x=0, y=0, z=0;
  float xm=0, ym=0, zm=0;
  float xgrav=0, ygrav=0.1, zgrav=0;
  
  public static final int DRAW_OBJECT_DOT = 0;
  public static final int DRAW_OBJECT_CIRCLE = 1;
  private int drawObject = DRAW_OBJECT_DOT;
  
  Blob(float _x, float _y) {
    x = _x;
    y = _y;
    z = 0;
  }
  
  Blob(float _x, float _y, color c) {
    x = _x;
    y = _y;
    z = 0;
    RED = c>>16&0xFF;
    GREEN = c>>8&0xFF;
    BLUE = c&0xFF;
  }
  
  Blob(float _x, float _y, color c, float _xm, float _ym, float _zm) {
    x = _x;
    y = _y;
    z = 0;
    xm = _xm;
    ym = _ym;
    zm = _zm;
    RED = c>>16&0xFF;
    GREEN = c>>8&0xFF;
    BLUE = c&0xFF;
  }
  
  void setDrawObject(int i) {
    drawObject = i;
  }
  
  void update(Vector collection) {
    xm+=xgrav;
    ym+=ygrav;
    zm+=zgrav;
    x+=xm;
    y+=ym;
    z+=zm;
    TRANS+=TRANSADD;
    if (TRANS<=0||z>300) collection.remove(this);
  }
  
  void draw(float transparency,int dO) {
    drawObject = dO;
    if (drawObject==DRAW_OBJECT_DOT) {
      stroke(RED,GREEN,BLUE,TRANS*transparency);
      point(x,y,z);
    } else if (drawObject==DRAW_OBJECT_CIRCLE) {
      pushMatrix();
      translate(x,y,z);
      noStroke();
      fill(RED,GREEN,BLUE,TRANS*transparency);
      ellipse(0,0,R,R);  
      popMatrix();
    }
  }
  
}
