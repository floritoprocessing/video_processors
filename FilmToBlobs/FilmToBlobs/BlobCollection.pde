class BlobCollection {
  
  private int wid=10;
  private int hei=10;
  private int nrOfBlobs=100;
  
  private Vector blobs;
  
  BlobCollection(int w, int h, float dens) {
    wid = w;
    hei = h;
    nrOfBlobs = (int)(wid*hei*dens);
    blobs = new Vector();
  }
  
  void createBlobs() {
    for (int i=0;i<nrOfBlobs;i++) {
      Blob b = new Blob(wid*random(1.0),hei*random(1.0));
      blobs.add(b);
    }
  }
  
  void createBlobsFromImage(PImage img) {
    for (int i=0;i<nrOfBlobs;i++) {
      float sx=wid*random(1.0);
      float sy=hei*random(1.0);
      color c = img.get((int)(img.width*sx/(float)wid),(int)(img.height*sy/(float)hei));
      float br = 2*brightness(c)/255.0;
      float xm=0;
      float ym=-br;
      float zm=3*br;
      Blob b = new Blob(sx,sy,c,xm,ym,zm);
      blobs.add(b);
    }
  }
  
  void update() {
    for (int i=0;i<blobs.size();i++) {
      ((Blob)(blobs.elementAt(i))).update(blobs);
    }
  }
  
  void draw(float transparency,int blobDrawObject) {
    for (int i=0;i<blobs.size();i++) {
      ((Blob)(blobs.elementAt(i))).draw(transparency,blobDrawObject);
    }
  }
  
}
