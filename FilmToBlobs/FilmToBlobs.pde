import java.util.Vector;

boolean SAVE_FRAMES = false;
int MAX_FRAMES_TO_SAVE = 60*25;
String OUTPUT_FILE_DOT = "C:\\Documents and Settings\\mgraf.ESAAD\\My Documents\\Processing\\FilmToBlobs\\output_dot\\astro_c_blobbed_dot_#####.tga";
String OUTPUT_FILE_CIRCLE = "C:\\Documents and Settings\\mgraf.ESAAD\\My Documents\\Processing\\FilmToBlobs\\output_circle\\astro_c_blobbed_circle_#####.tga";

String MOVIE_PATH = "";//D:\\Processing\\F\\FilmToBlobs\\data\\frames\\";
String MOVIE_BASENAME = "astro_c ";
int MOVIE_FRAMES = 5;//202;//202;

float BLOB_CREATION_DENSITY = 0.05;

// ----------------------------------------

int outFrameNr=0;
BlobCollection blobCollection;
ImageSequence inputMovie;


void setup() {
  
  size(768,576,P3D);
  noStroke();
  
  inputMovie = new ImageSequence(MOVIE_PATH,MOVIE_BASENAME,MOVIE_FRAMES);
  inputMovie.backAndForth();
  inputMovie.play();

  blobCollection = new BlobCollection(width,height,BLOB_CREATION_DENSITY); 
  
}



void draw() {
  if (outFrameNr<MAX_FRAMES_TO_SAVE) {
    
    blobCollection.createBlobsFromImage(inputMovie.getImage());    

    background(0,0,0);
    blobCollection.draw(0.3,Blob.DRAW_OBJECT_DOT);
    if (SAVE_FRAMES) saveFrame(OUTPUT_FILE_DOT);

    background(0,0,0);
    blobCollection.draw(0.3,Blob.DRAW_OBJECT_CIRCLE);
    if (SAVE_FRAMES) saveFrame(OUTPUT_FILE_CIRCLE);
    
    inputMovie.update();
    blobCollection.update();
    
    outFrameNr++;
  }
}
