class ImageSequence {
  
  private int nrOfFrames = 0;
  private static final int PLAY_MODE_LOOP_ON = 0;
  private static final int PLAY_MODE_LOOP_OFF = 1;
  private static final int PLAY_MODE_BACK_AND_FORTH = 2;
  
  private int playMode = PLAY_MODE_LOOP_OFF;
  
  private int currentFrame = 0;
  private int incFrame = 0;
  
  private PImage[] img;
  
  
  ImageSequence(String path, String basename, int frames) {
    nrOfFrames = frames;
    img = new PImage[nrOfFrames];
    for (int i=0;i<nrOfFrames;i++) {
      String filename = basename+nf(i+1,3)+".jpg";
      println("Loading "+filename);
      img[i] = loadImage(path+"\\"+filename);
    }
  }
  
  void invert() {
    for (int i=0;i<nrOfFrames;i++) {
      for (int p=0;p<img[i].pixels.length;p++) {
        int cc=img[i].pixels[p];
        int rr = 255 - (cc>>16&0xFF);
        int gg = 255 - (cc>>8&0xFF);
        int bb = 255 - (cc&0xFF);
        img[i].pixels[p] = rr<<16|gg<<8|bb;
      }
    }
  }
  
  void play() {
    incFrame = 1;
  }
  
  PImage getImage() {
    return img[currentFrame];
  }
  
  void update() {
    currentFrame += incFrame;
    
    if (playMode==PLAY_MODE_LOOP_ON) {
      if (currentFrame>=nrOfFrames) currentFrame=0;
      if (currentFrame<0) currentFrame=nrOfFrames-1;
    } else if (playMode==PLAY_MODE_LOOP_OFF) {
      if (currentFrame>=nrOfFrames) currentFrame=nrOfFrames-1;
      if (currentFrame<0) currentFrame=0;
    } else if (playMode==PLAY_MODE_BACK_AND_FORTH) {
      if (currentFrame>=nrOfFrames) {
        currentFrame = nrOfFrames-2;
        incFrame *= -1;
      }
      if (currentFrame<0) {
        currentFrame = 1;
        incFrame *= -1;
      }
    }
  }
  
  public int duration() {
    return nrOfFrames;
  }
  
  void loop() {
    playMode = PLAY_MODE_LOOP_ON;
  }
  
  void noLoop() {
    playMode = PLAY_MODE_LOOP_OFF;
  }
  
  void backAndForth() {
    playMode = PLAY_MODE_BACK_AND_FORTH;
  }
  
}
