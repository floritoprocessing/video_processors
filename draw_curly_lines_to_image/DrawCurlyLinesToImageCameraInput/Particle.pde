class Particle {
  float XMIN=0, XMAX=10;
  float YMIN=0, YMAX=10;
  
  color COLOR=color(0);
  float COLOR_MIXFAC_ORIG_TO_IMG=0.4; //0.8   // 0.0 .. 1.0 (the highter the number, the longer it takes to adjust color to image)
  float DRAWING_STRENGTH=0.7;  //0.4         // 0.0 .. 1.0
  
//  preset 1 - long strokes
//  float ACCELERATION_MAXIMUM=0.05;      // use in combination with SLOW_DOWN_FACTOR
//  float SLOW_DOWN_FACTOR=0.9995;         // 0.0 .. 1.0
  
//  preset 2 - small curls
  float ACCELERATION_MAXIMUM=0.25;
  float SLOW_DOWN_FACTOR=0.95;
  
//  preset 3 - sand
//  float ACCELERATION_MAXIMUM=5.0;
//  float SLOW_DOWN_FACTOR=0.97;

  // internal variables
  private float x=0, y=0;
  private float xmov, ymov;
  private float xacc=0, yacc=0;
  
  Particle(int _xmax, int _ymax) {
    XMAX=_xmax;
    YMAX=_ymax;
    x=random(XMAX);
    y=random(YMAX);
    xmov=random(0.5,1.5);
    xmov*=(random(1)<0.5?-1:1);
    ymov=random(0.5,1.5);
    ymov*=(random(1)<0.5?-1:1);
  }
  
  void update(PImage img) {
    xacc=random(-ACCELERATION_MAXIMUM,ACCELERATION_MAXIMUM);
    yacc=random(-ACCELERATION_MAXIMUM,ACCELERATION_MAXIMUM);
    xmov+=xacc;
    ymov+=yacc;
    xmov*=SLOW_DOWN_FACTOR;
    ymov*=SLOW_DOWN_FACTOR;
    x+=xmov;
    y+=ymov;
    while (x<XMIN) x+=XMAX;
    while (x>=XMAX) x-=XMAX;
    while (y<YMIN) y+=YMAX;
    while (y>=YMAX) y-=YMAX;
    color imgCol=img.get(width-int(x),int(y));
    float cdist=colorDistance(COLOR,imgCol);
    antialiasSet(x,y,COLOR,(1.0-cdist)*DRAWING_STRENGTH);
    COLOR=colorMix(COLOR,imgCol,COLOR_MIXFAC_ORIG_TO_IMG);
  }
}
