class ColorAndDrawingFunctions {
  ColorAndDrawingFunctions() {}
}

void antialiasSet(float x, float y, color c, float p) {
  int x1=floor(x);
  int x2=x1+1;
  int y1=floor(y);
  int y2=y1+1;
  
  float x2d=x-x1;
  float x1d=1.0-x2d;
  float y2d=y-y1;
  float y1d=1.0-y2d;
  
  softset(x1,y1,c,x1d*y1d*p);
  softset(x2,y1,c,x2d*y1d*p);
  softset(x1,y2,c,x1d*y2d*p);
  softset(x2,y2,c,x2d*y2d*p);
}

void softset(int x, int y, color c1, float p1) {
  set(x,y,colorMix(c1,get(x,y),p1));
}

int colorMix(int c1, int c2, float p1) {
  float p2=1.0-p1;
  int r1=c1>>16&0xff;
  int g1=c1>>8&0xff;
  int b1=c1&0xff;
  int r2=c2>>16&0xff;
  int g2=c2>>8&0xff;
  int b2=c2&0xff;
  int rr=int(p1*r1+p2*r2);
  int gg=int(p1*g1+p2*g2);
  int bb=int(p1*b1+p2*b2);
  int cc=rr<<16|gg<<8|bb;
  return cc;
}

float colorDistance(int c1, int c2) {
  int r1=c1>>16&0xff;
  int g1=c1>>8&0xff;
  int b1=c1&0xff;
  int r2=c2>>16&0xff;
  int g2=c2>>8&0xff;
  int b2=c2&0xff;
  return (abs(r1-r2)+abs(g1-g2)+abs(b1-b2))/765.0;
}
