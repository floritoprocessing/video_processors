import java.util.Vector;

int PARTICLES_AMOUNT=8000; //2000

Vector particles=new Vector();
Particle p;

import processing.video.*; 
Capture myCapture;


void setup() {
  size(640,480,P3D);
  myCapture=new Capture(this, width, height, 25);
  myCapture.start();
  for (int i=0;i<PARTICLES_AMOUNT;i++) {
    p=new Particle(width,height);
    particles.add(p);
  }
}

void captureEvent(Capture myCapture) { 
  myCapture.read(); 
  myCapture.updatePixels();
} 

void draw() {
  
  for (int i=0;i<particles.size();i++) {
    p=(Particle)(particles.elementAt(i));
    p.update(myCapture);
  }  
}
