import processing.video.*;
import java.util.Vector;

int PARTICLES_AMOUNT=2000;
String[] IMAGE_NAME={ "640-480-1.jpg" , "640-480-2.jpg" };

Vector particles=new Vector();
Particle p;
PImage[] img=new PImage[2];;
int img_nr=0;

Movie myMovie;

void setup() {
  size(640,480,P3D);
  for (int i=0;i<PARTICLES_AMOUNT;i++) {
    p=new Particle(width,height);
    particles.add(p);
  }
  
  for (int i=0;i<2;i++) img[i]=loadImage(IMAGE_NAME[i]);
  
  myMovie = new Movie(this, "vlak026.mov"); 
  myMovie.loop(); 
  
  background(255);
}


void draw() {
  myMovie.read();
  myMovie.updatePixels();
  for (int i=0;i<particles.size();i++) {
    p=(Particle)(particles.elementAt(i));
    //p.update(img[img_nr]);
    p.update(myMovie);
  }
}

void mousePressed() {
  img_nr=1-img_nr;
  println("following image number: "+img_nr);
}
