import java.util.Vector;

int PARTICLES_AMOUNT=8000; //2000

Vector particles=new Vector();
Particle p;
PImage[] img=new PImage[78];
PFont font;
int img_nr=0;
int loadImageNr=0;
boolean debug=true;
boolean loading = true;


void setup() {
  //framerate(15);

  size(640,480,P3D);//);//,P3D);
  
  font=loadFont("Arial-BoldMT-24.vlw");
  textFont(font);//,24);//,12);
  
  for (int i=0;i<PARTICLES_AMOUNT;i++) {
    p=new Particle(width,height);
    particles.add(p);
  }

  /*for (int i=0;i<78;i++) {
    String name="./data/vlak026 "+nf((i+1),2)+".jpg";
    img[i]=loadImage(name);
    println("loading frame "+i+": "+name);
  }*/

  background(255);
}

void mousePressed() {
  if (!loading) debug=!debug;
}


void draw() {
  if (loading) {
    background(255);
    stroke(0,0,0);
    fill(0,0,0);
    String name="./data/vlak026 "+nf((loadImageNr+1),2)+".jpg";
    fill(0);
    text("loading frame "+(loadImageNr+1)+"/"+img.length+": "+name,20,50);
    
    
    img[loadImageNr]=loadImage(name);
    img[loadImageNr].updatePixels();
    
    loadImageNr++;
    if (loadImageNr==img.length) loading = false;
  } else {
    fill(0);
    //text("drawing................................",10,20);
    img_nr++;
    if (img_nr==img.length) img_nr=0;
    //img[img_nr].loadPixels();
    for (int i=0;i<particles.size();i++) {
      p=(Particle)(particles.elementAt(i));
      p.update(img[img_nr]);
    }
    if (debug) {
      image(img[img_nr],0,0);
      fill(0);
      textAlign(CENTER);
      text("Click to show curly line-drawer",width/2+2,height/2+2);
      fill(255);
      text("Click to show curly line-drawer",width/2,height/2);
      
    }
  }
  //image(img[img_nr],0,0);
}
